defmodule RemoteSmsWeb.AccountView do
  use RemoteSmsWeb, :view
  alias RemoteSmsWeb.AccountView

  def render("success_login.json", %{user: user}) do
    %{data: render_one(user, AccountView, "user.json")}
  end

  def render("user.json", %{account: user}) do
    %{token: token, user: current_user} = user
    %{token: token,
      user: %{
        full_name: current_user.full_name,
        email: current_user.email
      }
    }
  end

###########################
  def render("list_devices.json", %{devices: devices}) do
    %{data: render_many(devices, AccountView, "device.json")}
  end

  def render("show_device.json", %{device: device}) do
    %{data: render_one(device, AccountView, "device.json")}
  end

  def render("device.json", %{account: device}) do
    %{id: device.id,
      name: device.name,
      serial: device.serial,}
  end

end
