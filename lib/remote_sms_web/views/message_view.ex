defmodule RemoteSmsWeb.MessageView do
  use RemoteSmsWeb, :view
  alias RemoteSmsWeb.MessageView

  def render("index.json", %{messages: messages}) do
    %{data: render_many(messages, MessageView, "message.json")}
  end

  def render("show.json", %{message: message}) do
    %{data: render_one(message, MessageView, "message.json")}
  end

  def render("message.json", %{message: message}) do
    %{id: message.id,
      original_id: message.original_id,
      original_thread_id: message.original_thread_id,
      body: message.body,
      date_sent: message.date_sent,
      thread_id: message.thread_id,
      type: message.type}
  end
end
