defmodule RemoteSmsWeb.ThreadView do
  use RemoteSmsWeb, :view
  alias RemoteSmsWeb.ThreadView
  alias RemoteSmsWeb.MessageView


  def render("index.json", %{threads: threads}) do
    %{data: render_many(threads, ThreadView, "thread_view.json")}
  end

  def render("show.json", %{thread: thread}) do
    %{data: render_one(thread, ThreadView, "thread.json")}
  end

  def render("thread.json", %{thread: thread}) do
    %{id: thread.id,
      original_id: thread.original_id,
      recipient: thread.recipient,}
  end

  def render("thread_view.json", %{thread: thread}) do
    %{id: thread.id,
      original_id: thread.original_id,
      recipient: thread.recipient,
      snippet: thread.snippet,
      last_msg_date: thread.last_msg_date}
  end

  def render("index.json", %{messages: messages}) do
    %{data: render_many(messages, MessageView, "message.json")}
  end

  def render("show.json", %{message: message}) do
    %{data: render_one(message, MessageView, "message.json")}
  end



end
