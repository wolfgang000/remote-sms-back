defmodule RemoteSmsWeb.MessageController do
  use RemoteSmsWeb, :controller

  alias RemoteSms.Sms
  alias RemoteSms.Sms.Message
  alias RemoteSms.Sms.RequestThread

  action_fallback RemoteSmsWeb.FallbackController

  def index(conn, _params) do
    messages = Sms.list_messages()
    render(conn, "index.json", messages: messages)
  end


  def get_thread_params_from_message_params(message_params) do
    with cs <- %RequestThread{} |> RequestThread.changeset(message_params) do
      if cs.valid? do
        { :ok, cs.changes }
      else
        { :error, cs }
      end
    end
  end

  def get_or_create_thread_by_by_original_id(%{ original_thread_id: original_id, address: recipient }) do
    with thread <- Sms.get_thread_by_original_id(original_id) do
      if thread != nil do
        {:ok, thread, false}
      else
        %{ original_id: original_id, recipient: recipient }
        |> Sms.create_thread()
        |> Tuple.append(true)
      end
    end
  end

  def create(conn, %{"message" => message_params}) do

    with {:ok, thread_params } <- get_thread_params_from_message_params(message_params),
      {:ok, thread, was_thread_created } <- get_or_create_thread_by_by_original_id(thread_params),
      {:ok, %Message{} = message } <- Sms.create_message( message_params |>  Map.put("thread_id", thread.id))
    do

      if was_thread_created do
        RemoteSmsWeb.Endpoint.broadcast!("chat:lobby", "thread:new", %{ thread: thread, message: message })
      else
        RemoteSmsWeb.Endpoint.broadcast!("chat:lobby", "message:incoming", message)
      end

      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.message_path(conn, :show, message))
      |> render("show.json", message: message)
    end
  end

  def show(conn, %{"id" => id}) do
    message = Sms.get_message!(id)
    render(conn, "show.json", message: message)
  end

  def update(conn, %{"id" => id, "message" => message_params}) do
    message = Sms.get_message!(id)

    with {:ok, %Message{} = message} <- Sms.update_message(message, message_params) do
      render(conn, "show.json", message: message)
    end
  end

  def delete(conn, %{"id" => id}) do
    message = Sms.get_message!(id)

    with {:ok, %Message{}} <- Sms.delete_message(message) do
      send_resp(conn, :no_content, "")
    end
  end
end
