defmodule RemoteSmsWeb.AccountController do
  use RemoteSmsWeb, :controller

  alias RemoteSms.Accounts

  action_fallback RemoteSmsWeb.FallbackController


  def login(conn, %{"user" => %{"email" => email, "password" => password}}) do
    # TODO: Add validations and proper error messages
    with  {:ok, user } <- Accounts.authenticate_user(email, password),
          {:ok, user } <- Accounts.update_user_last_login_date(user)
    do
      user
      |> login_reply(conn)
    end
  end

  def get_users_devices(conn, _ ) do
    user = RemoteSms.Guardian.Plug.current_resource(conn)
    user_with_devices = Accounts.preload_devices_from_user(user)

    conn
    |> render("list_devices.json", devices: user_with_devices.devices)
  end

  defp login_reply(user, conn) do
    {:ok, token, _ } = RemoteSms.Guardian.encode_and_sign(user)
    conn
    |> put_status(:ok)
    |> render("success_login.json", user: %{token: token, user: user})
  end





end
