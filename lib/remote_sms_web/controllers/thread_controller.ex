defmodule RemoteSmsWeb.ThreadController do
  use RemoteSmsWeb, :controller

  alias RemoteSms.Sms
  alias RemoteSms.Sms.Thread
  alias RemoteSms.Sms.Message


  action_fallback RemoteSmsWeb.FallbackController

  def index(conn, _params) do
    threads = Sms.list_threads_view()
    render(conn, "index.json", threads: threads)
  end

  def create(conn, %{"thread" => thread_params}) do
    with {:ok, %Thread{} = thread} <- Sms.create_thread(thread_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.thread_path(conn, :show, thread))
      |> render("show.json", thread: thread)
    end
  end

  def show(conn, %{"id" => id}) do
    thread = Sms.get_thread_view!(id)
    render(conn, "show.json", thread: thread)
  end

  def update(conn, %{"id" => id, "thread" => thread_params}) do
    thread = Sms.get_thread!(id)

    with {:ok, %Thread{} = thread} <- Sms.update_thread(thread, thread_params) do
      render(conn, "show.json", thread: thread)
    end
  end

  def delete(conn, %{"id" => id}) do
    thread = Sms.get_thread!(id)

    with {:ok, %Thread{}} <- Sms.delete_thread(thread) do
      send_resp(conn, :no_content, "")
    end
  end

  def add_message(conn, %{"id" => id, "message" => message_params}) do

    with {:ok, %Message{} = message} <- Sms.create_message(message_params |> Map.put_new( "thread_id", id) ) do
      RemoteSmsWeb.Endpoint.broadcast!("chat:lobby", "message:incoming", message)
      conn
      |> put_status(:created)
      |> render("show.json", message: message)
    end
  end

  def create_messages(conn, %{"id" => id, "messages" => messages_params}) do
    with { _ , _ } <- Sms.create_bulk_messages(messages_params, id) do
      conn
      |> put_status(:created)
    end
  end

  def create_bulk_threads(conn, %{"threads" => threads_params}) do
    with { _ , _ } <- Sms.create_bulk_threads(threads_params) do
      conn
      |> put_status(:created)
    end
  end


  def get_messages(conn, %{"id" => id}) do
    messages = Sms.get_messages_by_thread!(id)
    render(conn, "index.json", messages: messages)
  end


end
