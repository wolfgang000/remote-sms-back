defmodule RemoteSmsWeb.Router do
  use RemoteSmsWeb, :router


  pipeline :auth do
    plug RemoteSmsWeb.Accounts.AuthAccessPipeline
  end

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
    plug :fetch_flash
  end

  scope "/", RemoteSmsWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", RemoteSmsWeb do
    pipe_through [:api, :auth]

    resources "/threads", ThreadController, except: [:new, :edit]
    get "/threads/:id/messages", ThreadController, :get_messages
    post "/threads/:id/messages", ThreadController, :add_message
    post "/threads/:id/messages_bulk", ThreadController, :create_messages
    post "/threads_bulk", ThreadController, :create_bulk_threads
    resources "/messages", MessageController, except: [:new, :edit]

    # Accounts
    get "/accounts/me/devices", AccountController, :get_users_devices
  end

  scope "/api", RemoteSmsWeb do
    pipe_through [:api ]
    # Accounts
    post "/accounts/login", AccountController, :login
  end

end
