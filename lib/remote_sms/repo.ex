defmodule RemoteSms.Repo do
  use Ecto.Repo,
    otp_app: :remote_sms,
    adapter: Ecto.Adapters.Postgres
end
