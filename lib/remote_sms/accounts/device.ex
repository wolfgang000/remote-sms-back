defmodule RemoteSms.Accounts.Device do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:id, :name, :serial, ]}
  schema "devices" do
    field :name, :string
    field :serial, :string
    belongs_to :user, RemoteSms.Accounts.User

    timestamps()
  end

  @doc false
  def changeset(device, attrs) do
    device
    |> cast(attrs, [:serial, :name, :user_id])
    |> foreign_key_constraint(:user_id)
    |> validate_required([:serial, :name])
  end
end
