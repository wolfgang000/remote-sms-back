
defmodule RemoteSmsWeb.Accounts.AuthAccessPipeline do
  use Guardian.Plug.Pipeline, otp_app: :remote_sms

  # If there is a session token, validate it
  plug Guardian.Plug.VerifySession, claims: %{"typ" => "access"}

  # If there is an authorization header, validate it
  plug Guardian.Plug.VerifyHeader, claims: %{"typ" => "access"}

  # Makes sure that a token was found and is valid
  plug Guardian.Plug.EnsureAuthenticated

  # Load the user if either of the verifications worked
  plug Guardian.Plug.LoadResource, allow_blank: true
end

