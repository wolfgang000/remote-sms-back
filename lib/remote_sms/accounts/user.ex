defmodule RemoteSms.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset


  schema "users" do
    field :email, :string
    field :password, :string
    field :full_name, :string
    field :last_login, :utc_datetime
    has_many :devices, RemoteSms.Accounts.Device


    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :password, :last_login, :full_name])
    |> validate_required([:email, :password])
  end
end
