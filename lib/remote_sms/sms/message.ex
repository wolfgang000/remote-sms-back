defmodule RemoteSms.Sms.Message do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:id, :original_id, :original_thread_id, :body, :date_sent, :type, :thread_id]}
  schema "messages" do
    field :original_id, :integer
    field :original_thread_id, :integer
    field :body, :string
    field :date_sent, :utc_datetime
    field :type, :integer
    field :read, :boolean
    belongs_to :thread, RemoteSms.Sms.Thread

    timestamps()
  end

  @types %{
    inbox: 1,
    sent: 2,
  }
  def types, do: @types

  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, [:original_id,  :original_thread_id, :body, :date_sent, :type, :thread_id])
    |> foreign_key_constraint(:thread_id)
    |> validate_required([:original_id, :original_thread_id, :body, :date_sent, :type, ])
    |> validate_inclusion(:type, Map.values(@types))
  end
end
