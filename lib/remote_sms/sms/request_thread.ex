defmodule RemoteSms.Sms.RequestThread do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:id, :original_thread_id, :address ]}
  schema "request_thread" do
    field :original_thread_id, :integer
    field :address, :string
  end

  @doc false
  def changeset(request_thread, attrs) do
    request_thread
    |> cast(attrs, [:original_thread_id, :address, ])
    |> validate_required([:original_thread_id, :address, ])
  end
end
