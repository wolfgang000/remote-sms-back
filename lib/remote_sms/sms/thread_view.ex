defmodule RemoteSms.Sms.Thread_View do
  use Ecto.Schema

  @derive {Jason.Encoder, only: [:id, :original_id, :snippet, :last_msg_date, ]}
  schema "threads_view" do
    field :original_id, :integer
    field :recipient, :string
    field :last_msg_date, :utc_datetime
    field :snippet, :string


    timestamps()
  end
end
