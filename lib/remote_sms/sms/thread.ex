defmodule RemoteSms.Sms.Thread do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:id, :original_id, :recipient]}
  schema "threads" do
    field :original_id, :integer
    field :recipient, :string
    has_many :messages, RemoteSms.Sms.Message

    timestamps()
  end

  @doc false
  def changeset(thread, attrs) do
    thread
    |> cast(attrs, [:original_id, :recipient])
    |> validate_required([:original_id, :recipient])
  end
end
