# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :remote_sms,
  ecto_repos: [RemoteSms.Repo]

# Configures the endpoint
config :remote_sms, RemoteSmsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "cJmjUfgp19EYY0gcG8oSRD4bVzfOeIws51u5hQaAcMKZe+25LGF0QFzeHnEtjkZi",
  render_errors: [view: RemoteSmsWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: RemoteSms.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Config Guardian for JWT Generation
# TODO: Replace hardcoded key with env variable
config :remote_sms, RemoteSms.Guardian,
  issuer: "remote_sms",
  secret_key: "cbcydEmGfDh+xIQ9Q3UAfAD1layQ1e7+F2aSl4XxcodTCgnJWrzHyWbyBvnHLrhj"

config :remote_sms, RemoteSmsWeb.Accounts.AuthAccessPipeline,
  module: RemoteSms.Guardian,
  error_handler: RemoteSmsWeb.Accounts.AuthErrorHandler


# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
