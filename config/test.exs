use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :remote_sms, RemoteSmsWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :remote_sms, RemoteSms.Repo,
  username: System.get_env("TEST_DB_USER") || "test_user",
  password: System.get_env("TEST_DB_PASSWORD") || "test_password",
  database: System.get_env("TEST_DB_NAME") || "remote_sms_test1",
  hostname: System.get_env("TEST_DB_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
