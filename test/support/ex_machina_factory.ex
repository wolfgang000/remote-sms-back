defmodule RemoteSms.Factory do
  # with Ecto
  use ExMachina.Ecto, repo: RemoteSms.Repo

  def user_factory do
    %RemoteSms.Accounts.User{
      full_name: "Jane Smith",
      email: sequence(:email, &"email-#{&1}@example.com"),
    }
  end

  def user_device_factory do
    %RemoteSms.Accounts.Device{
      name: sequence("name"),
      serial: sequence("serial"),
      user: build(:user),
    }
  end


end
