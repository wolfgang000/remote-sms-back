

defmodule IsSorted do
    def desc([ _ | [] ]), do: true
    def desc([head|tail]) do
        [ next_item | _ ] = tail
        if head >= next_item do
            desc(tail)
        else
            false
        end
    end
end
