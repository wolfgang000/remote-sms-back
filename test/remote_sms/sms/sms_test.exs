defmodule RemoteSms.SmsTest do
  use RemoteSms.DataCase

  alias RemoteSms.Sms

  describe "threads" do
    alias RemoteSms.Sms.Thread

    @valid_attrs %{ original_id: 12, recipient: "1234567",}
    @update_attrs %{ original_id: 13, recipient: "1234567",}
    @invalid_attrs %{ original_id: nil, recipient: nil,}

    def thread_fixture(attrs \\ %{}) do
      {:ok, thread} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Sms.create_thread()

      thread
    end

    test "list_threads/0 returns all threads" do
      thread = thread_fixture()
      assert Sms.list_threads() == [thread]
    end

    test "get_thread!/1 returns the thread with given id" do
      thread = thread_fixture()
      assert Sms.get_thread!(thread.id) == thread
    end

    test "create_thread/1 with valid data creates a thread" do
      assert {:ok, %Thread{} = thread} = Sms.create_thread(@valid_attrs)
      assert thread.original_id == @valid_attrs.original_id
    end

    test "create_thread/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Sms.create_thread(@invalid_attrs)
    end

    test "update_thread/2 with valid data updates the thread" do
      thread = thread_fixture()
      assert {:ok, %Thread{} = thread} = Sms.update_thread(thread, @update_attrs)
      assert thread.original_id == @update_attrs.original_id
    end

    test "update_thread/2 with invalid data returns error changeset" do
      thread = thread_fixture()
      assert {:error, %Ecto.Changeset{}} = Sms.update_thread(thread, @invalid_attrs)
      assert thread == Sms.get_thread!(thread.id)
    end

    test "delete_thread/1 deletes the thread" do
      thread = thread_fixture()
      assert {:ok, %Thread{}} = Sms.delete_thread(thread)
      assert_raise Ecto.NoResultsError, fn -> Sms.get_thread!(thread.id) end
    end

    test "change_thread/1 returns a thread changeset" do
      thread = thread_fixture()
      assert %Ecto.Changeset{} = Sms.change_thread(thread)
    end
  end

  describe "messages" do
    alias RemoteSms.Sms.Message

    @valid_attrs %{original_id: 1, body: "some body", date_sent: ~N[2010-04-17 14:00:00], type: 1, original_thread_id: 5,}
    @update_attrs %{original_id: 2, body: "some updated body", date_sent: ~N[2011-05-18 15:01:01], type: 2, original_thread_id: 6,}
    @invalid_attrs %{original_id: nil, body: nil, date_sent: nil, type: nil}

    def message_fixture(attrs \\ %{}) do
      {:ok, message} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Sms.create_message()

      message
    end

    test "list_messages/0 returns all messages" do
      message = message_fixture()
      assert Sms.list_messages() == [message]
    end

    test "get_message!/1 returns the message with given id" do
      message = message_fixture()
      assert Sms.get_message!(message.id) == message
    end

    test "create_message/1 with valid data creates a message" do
      assert {:ok, %Message{} = message} = Sms.create_message(@valid_attrs)
      assert message.body == "some body"
      {:ok, date_test } = DateTime.from_naive(~N[2010-04-17 14:00:00], "Etc/UTC")

      assert message.date_sent == date_test
      assert message.type == @valid_attrs.type
    end

    test "create_message/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Sms.create_message(@invalid_attrs)
    end

    test "update_message/2 with valid data updates the message" do
      message = message_fixture()
      assert {:ok, %Message{} = message} = Sms.update_message(message, @update_attrs)
      assert message.body == "some updated body"
      {:ok, date_test } = DateTime.from_naive(~N[2011-05-18 15:01:01], "Etc/UTC")
      assert message.date_sent ==  date_test
      assert message.type == @update_attrs.type
    end

    test "update_message/2 with invalid data returns error changeset" do
      message = message_fixture()
      assert {:error, %Ecto.Changeset{}} = Sms.update_message(message, @invalid_attrs)
      assert message == Sms.get_message!(message.id)
    end

    test "delete_message/1 deletes the message" do
      message = message_fixture()
      assert {:ok, %Message{}} = Sms.delete_message(message)
      assert_raise Ecto.NoResultsError, fn -> Sms.get_message!(message.id) end
    end

    test "change_message/1 returns a message changeset" do
      message = message_fixture()
      assert %Ecto.Changeset{} = Sms.change_message(message)
    end
  end
end
