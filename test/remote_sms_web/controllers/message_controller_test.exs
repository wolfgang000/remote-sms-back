defmodule RemoteSmsWeb.MessageControllerTest do
  use RemoteSmsWeb.ConnCase
  use Phoenix.ChannelTest


  alias RemoteSms.Sms

  @create_message_attrs %{
    original_id: 1,
    original_thread_id: 5,
    body: "some body",
    date_sent: ~N[2010-04-17 14:00:00],
    type: 1,
    address: "0500228800",
  }
  # @update_message_attrs %{
  #   original_id: 1,
  #   body: "some updated body",
  #   date_sent: ~N[2011-05-18 15:01:01],
  #   type: 2
  # }
  # @invalid_attrs %{original_id: nil, body: nil, date_sent: nil, type: nil}

  def fixture(:message) do
    {:ok, message} = Sms.create_message(@create_message_attrs)
    message
  end


  @create_thread_attrs %{
    original_id: 123,
    recipient: "1234567",
  }
  # @update_thread_attrs %{
  #   original_id: 126,
  #   recipient: "1234567",
  # }
  # @invalid_attrs %{original_id: nil,}

  def fixture(:thread) do
    {:ok, thread} = Sms.create_thread(@create_thread_attrs)
    thread
  end

  def fixture(:messages_with_thread) do
    {:ok, thread} = Sms.create_thread(@create_thread_attrs)

    messages = for n <- 1..9 do
      {:ok, date_test } = ~N[2010-04-17 14:00:00] |> DateTime.from_naive("Etc/UTC")
      {:ok, message} = Sms.create_message(
        %{@create_message_attrs |
          original_id: n,
          date_sent:  date_test |> DateTime.add(n*24*60*60, :second),
        } |> Map.put(:thread_id, thread.id))
      message
    end

    {thread, messages}
  end


  setup %{conn: conn} do
    conn = put_req_header(conn, "accept", "application/json")
    user = RemoteSms.Factory.insert(:user)
    {:ok, token, _ } = RemoteSms.Guardian.encode_and_sign(user)
    conn_with_auth_token = conn
    |> put_req_header("authorization", "bearer: " <> token)
    {:ok, conn: conn, conn_with_auth_token: conn_with_auth_token, auth_user: user }
  end


  describe "index" do
    setup [:create_thread]

    test "lists all messages", %{conn_with_auth_token: conn,  thread: thread} do
      conn = get(conn, Routes.thread_path(conn, :get_messages, thread.id))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "get messages list" do
    setup [:create_messages_with_thread]

    test "lists all messages of thread", %{conn_with_auth_token: conn,  thread: thread, messages: messages} do
      conn = get(conn, Routes.thread_path(conn, :get_messages, thread.id))
      assert length(json_response(conn, 200)["data"]) == length(messages)
    end
    test "Check if they are order by date", %{conn_with_auth_token: conn,  thread: thread, messages: messages} do
      conn = get(conn, Routes.thread_path(conn, :get_messages, thread.id))
      messages_response = json_response(conn, 200)["data"]
      assert length(messages_response) == length(messages)
      messages_dates = Enum.map(messages_response,
        fn message ->
        {:ok, date_sent, 0} = DateTime.from_iso8601(message["date_sent"])
        date_sent
        end
      )
      assert IsSorted.desc(messages_dates)

    end


  end

  describe "Sync messages" do
    setup [:create_thread, :connect_socket]

    test "Create Thread and upload messages", %{conn_with_auth_token: conn,  thread: thread } do
      messages = [
        %{@create_message_attrs | original_id: 1, },
        %{@create_message_attrs | original_id: 2, },
        %{@create_message_attrs | original_id: 3, },
      ]

      conn = post(conn, Routes.thread_path(conn, :create_messages, thread.id),  messages: messages)
      assert conn.status == 201

      conn = get(conn, Routes.thread_path(conn, :get_messages, thread.id))
      assert length(json_response(conn, 200)["data"]) == length(messages)

    end

    test "Add new message", %{conn_with_auth_token: conn, thread: thread } do
      conn = post(conn, Routes.thread_path(conn, :add_message, thread.id), message: @create_message_attrs)
      assert conn.status == 201
      message = json_response(conn, 201)["data"]
      assert %{"id" => id} = message
      assert_broadcast "message:incoming", message
    end

  end


  describe "create message" do
    setup [ :create_thread, :connect_socket]
    test "without thread created", %{conn_with_auth_token: conn, } do
      conn = post(conn, Routes.message_path(conn, :create), message:  @create_message_attrs)

      %{ :original_id => original_id, :original_thread_id =>  original_thread_id, } =  @create_message_attrs

      response_msg = json_response(conn, 201)["data"]
      assert %{
        "original_id" => ^original_id,
        "original_thread_id" => ^original_thread_id,
      } = response_msg

      %{ "thread_id" =>  thread_id, "id" => message_id } = response_msg

      # Test socket response
      assert_receive %Phoenix.Socket.Broadcast{topic: "chat:lobby", event: "thread:new", payload: raw_payload}

      payload = raw_payload |> Jason.encode!() |> Jason.decode!()

      assert %{
        "message" => %{"original_id" => ^original_id },
        "thread" => %{"id" => ^thread_id, "original_id" => ^original_thread_id, }
      } = payload


      conn = get(conn, Routes.thread_path(conn, :get_messages, thread_id))
      messages_response = json_response(conn, 200)["data"]

      assert length(messages_response) == 1

      assert %{
               "id" => ^message_id,
               "original_id" => ^original_id,
               "original_thread_id" => ^original_thread_id,
             } = List.first(messages_response)
    end

    test "with thread created", %{conn_with_auth_token: conn, thread: thread, } do
      conn = post(conn, Routes.message_path(conn, :create), message:  %{ @create_message_attrs | original_thread_id: thread.original_id })

      %{ :original_id => original_id } =  @create_message_attrs
      original_thread_id = thread.original_id

      response_msg = json_response(conn, 201)["data"]
      assert %{
        "original_id" => ^original_id,
        "original_thread_id" => ^original_thread_id,
      } = response_msg

      %{ "thread_id" =>  thread_id, "id" => message_id } = response_msg

      # Test socket response
      assert_receive %Phoenix.Socket.Broadcast{topic: "chat:lobby", event: "message:incoming", payload: raw_payload}

      message_payload = raw_payload |> Jason.encode!() |> Jason.decode!()

      assert %{
        "id" => ^message_id,
        "original_id" => ^original_id,
      } = message_payload


      conn = get(conn, Routes.thread_path(conn, :get_messages, thread_id))
      messages_response = json_response(conn, 200)["data"]

      assert length(messages_response) == 1

      assert %{
               "id" => ^message_id,
               "original_id" => ^original_id,
               "original_thread_id" => ^original_thread_id,
             } = List.first(messages_response)
    end


  end


  #   test "renders errors when data is invalid", %{conn: conn} do
  #     conn = post(conn, Routes.message_path(conn, :create), message: @invalid_attrs)
  #     assert json_response(conn, 422)["errors"] != %{}
  #   end
  # end

  # describe "update message" do
  #   setup [:create_message]

  #   test "renders message when data is valid", %{conn: conn, message: %Message{id: id} = message} do
  #     conn = put(conn, Routes.message_path(conn, :update, message), message: @update_attrs)
  #     assert %{"id" => ^id} = json_response(conn, 200)["data"]

  #     conn = get(conn, Routes.message_path(conn, :show, id))

  #     assert %{
  #              "id" => id,
  #              "body" => "some updated body",
  #              "date_sent" => "2011-05-18T15:01:01",
  #              "type" => 43
  #            } = json_response(conn, 200)["data"]
  #   end

  #   test "renders errors when data is invalid", %{conn: conn, message: message} do
  #     conn = put(conn, Routes.message_path(conn, :update, message), message: @invalid_attrs)
  #     assert json_response(conn, 422)["errors"] != %{}
  #   end
  # end

  # describe "delete message" do
  #   setup [:create_message]

  #   test "deletes chosen message", %{conn: conn, message: message} do
  #     conn = delete(conn, Routes.message_path(conn, :delete, message))
  #     assert response(conn, 204)

  #     assert_error_sent 404, fn ->
  #       get(conn, Routes.message_path(conn, :show, message))
  #     end
  #   end
  # end

  defp create_message(_) do
    message = fixture(:message)
    {:ok, message: message}
  end

  defp create_thread(_) do
    thread = fixture(:thread)
    {:ok, thread: thread}
  end

  defp connect_socket(_) do
    {:ok, _, socket} =
      socket(RemoteSmsWeb.UserSocket, "user_id", %{some: :assign})
      |> subscribe_and_join(RemoteSmsWeb.ChatChannel, "chat:lobby")

    {:ok, socket: socket, }
  end

  defp create_messages_with_thread(_) do
    {thread, messages} = fixture(:messages_with_thread)
    {:ok, thread: thread, messages: messages}
  end
end
