defmodule RemoteSmsWeb.AccountControllerTest do
  use RemoteSmsWeb.ConnCase
  use Phoenix.ChannelTest


  alias RemoteSms.Accounts

  @valid_attrs %{email: "test@mail.com", password: "some password", full_name: "test user"}

  def fixture(:user) do
    {:ok, user} = Accounts.create_user(@valid_attrs)
    user
  end

  defp create_user(_) do
    user = fixture(:user)
    {:ok, user: user}
  end

  setup %{conn: conn} do
    conn = put_req_header(conn, "accept", "application/json")
    user = RemoteSms.Factory.insert(:user)
    {:ok, token, _ } = RemoteSms.Guardian.encode_and_sign(user)
    conn_with_auth_token = conn
    |> put_req_header("authorization", "bearer: " <> token)
    {:ok, conn: conn, conn_with_auth_token: conn_with_auth_token, auth_user: user }
  end

  describe "login" do
    setup [:create_user]

    test "with valid credentials", %{conn: conn, } do
      conn = post(conn, Routes.account_path(conn, :login ),  user: @valid_attrs )
      assert conn.status == 200
      response_data = json_response(conn, 200)["data"]
      assert %{"token" => token} = response_data
      assert token != nil
      assert %{
        "user" => %{
          "email" => "test@mail.com",
          "full_name" => "test user",
        }
      } = response_data

    end

    test "check valid login's last date", %{conn: conn, user: user,} do
      conn = post(conn, Routes.account_path(conn, :login ),  user: @valid_attrs )
      assert conn.status == 200
      logged_user = Accounts.get_user!(user.id)
      assert DateTime.diff(DateTime.utc_now(), logged_user.last_login, :millisecond) < 1000
    end
  end

  describe "me" do
    setup [:create_user]

    test "get devices", %{conn_with_auth_token: conn, auth_user: user} do
      device = RemoteSms.Factory.insert(:user_device, %{user: user})

      conn = get(conn, Routes.account_path(conn, :get_users_devices))
      assert conn.status == 200
      devices = json_response(conn, 200)["data"]
      assert devices |> length() == 1
    end

  end


end
