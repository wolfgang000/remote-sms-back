defmodule RemoteSmsWeb.ThreadControllerTest do
  use RemoteSmsWeb.ConnCase

  alias RemoteSms.Sms
  alias RemoteSms.Sms.Thread

  @create_message_attrs %{
    original_id: 1,
    original_thread_id: 5,
    body: "some body",
    date_sent: ~N[2010-04-17 14:00:00],
    type: 1
  }

  @create_attrs %{
    original_id: 12,
    recipient: "1234567",
  }
  @update_attrs %{
    original_id: 14,
    recipient: "1234567",
  }
  @invalid_attrs %{original_id: nil, recipient: nil, }

  def fixture(:thread) do
    {:ok, thread} = Sms.create_thread(@create_attrs)
    thread
  end

  def fixture(:threads_with_message) do
    threads = for n <- 1..9 do
      {:ok, thread } = Sms.create_thread(%{ @create_attrs | original_id: n })
      {:ok, date_test } = ~N[2010-04-17 14:00:00] |> DateTime.from_naive("Etc/UTC")
      {:ok, _ } = Sms.create_message(
        %{@create_message_attrs |
          original_id: n,
          original_thread_id: n,
          date_sent: date_test |> DateTime.add(n*24*60*60, :second),
        } |> Map.put(:thread_id, thread.id))
      thread
    end
    threads
  end

  setup %{conn: conn} do
    conn = put_req_header(conn, "accept", "application/json")
    user = RemoteSms.Factory.insert(:user)
    {:ok, token, _ } = RemoteSms.Guardian.encode_and_sign(user)
    conn_with_auth_token = conn
    |> put_req_header("authorization", "bearer: " <> token)
    {:ok, conn: conn, conn_with_auth_token: conn_with_auth_token, auth_user: user }
  end

  describe "index" do

    test "lists all threads", %{conn_with_auth_token: conn, } do
      conn = get(conn, Routes.thread_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "get threads list" do
    setup [:create_threads_with_message, ]

    test "lists all threads", %{conn_with_auth_token: conn,  threads: threads } do
      conn = get(conn, Routes.thread_path(conn, :index))
      assert length(json_response(conn, 200)["data"]) == length(threads)
    end
    test "Check if they are order by date", %{conn_with_auth_token: conn,  threads: threads } do
      conn = get(conn, Routes.thread_path(conn, :index))
      threads_response = json_response(conn, 200)["data"]
      assert length(threads_response) == length(threads)

      threads_dates = Enum.map(threads_response,
        fn message ->
        {:ok, date_sent, 0} = DateTime.from_iso8601(message["last_msg_date"])
        date_sent
        end
      )
      assert IsSorted.desc(threads_dates)
    end
  end


  describe "create thread" do

    test "renders thread when data is valid", %{conn_with_auth_token: conn, } do
      conn = post(conn, Routes.thread_path(conn, :create), thread: @create_attrs)
      assert %{"id" => id, "original_id" => original_id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.thread_path(conn, :show, id))

      assert %{
               "id" => ^id,
               "original_id" => ^original_id,
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn_with_auth_token: conn, } do
      conn = post(conn, Routes.thread_path(conn, :create), thread: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end

    test "bulk create", %{conn_with_auth_token: conn, } do
      threads = [
        %{
          original_id: 1,
        },
        %{
          original_id: 2
        },
        %{
          original_id: 3
        },
        %{
          original_id: 4
        },
      ]
      conn = post(conn, Routes.thread_path(conn, :create_bulk_threads), threads: threads)
      assert conn.status == 201

      conn = get(conn, Routes.thread_path(conn, :index))
      assert length(json_response(conn, 200)["data"]) == length(threads)
    end

  end

  describe "update thread" do
    setup [:create_thread, ]

    test "renders thread when data is valid", %{conn_with_auth_token: conn, thread: %Thread{id: id} = thread} do
      conn = put(conn, Routes.thread_path(conn, :update, thread), thread: @update_attrs)
      assert %{"id" => ^id, "original_id" => original_id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.thread_path(conn, :show, id))

      assert %{
               "id" => ^id,
               "original_id" => ^original_id,
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn_with_auth_token: conn, thread: thread} do
      conn = put(conn, Routes.thread_path(conn, :update, thread), thread: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete thread" do
    setup [:create_thread, ]

    test "deletes chosen thread", %{conn_with_auth_token: conn, thread: thread} do
      conn = delete(conn, Routes.thread_path(conn, :delete, thread))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.thread_path(conn, :show, thread))
      end
    end
  end

  defp create_thread(_) do
    thread = fixture(:thread)
    {:ok, thread: thread}
  end

  defp create_threads_with_message(_) do
    threads = fixture(:threads_with_message)

    {:ok, threads: threads }
  end

end
