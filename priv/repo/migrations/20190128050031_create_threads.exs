defmodule RemoteSms.Repo.Migrations.CreateThreads do
  use Ecto.Migration

  def change do
    create table(:threads) do
      add :original_id, :integer, null: false
      add :recipient, :string
      timestamps()
    end

  end
end
