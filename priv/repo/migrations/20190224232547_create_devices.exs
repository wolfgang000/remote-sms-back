defmodule RemoteSms.Repo.Migrations.CreateDevices do
  use Ecto.Migration

  def change do
    create table(:devices) do
      add :serial, :string
      add :name, :string
      add :user_id, references(:users, on_delete: :delete_all) #, null: false

      timestamps()
    end

  end
end
