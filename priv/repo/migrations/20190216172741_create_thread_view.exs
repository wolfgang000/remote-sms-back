defmodule RemoteSms.Repo.Migrations.CreateThreadView do
  # base on: https://stackoverflow.com/a/2111420
  use Ecto.Migration
  def up do
    execute("""
    CREATE VIEW threads_view AS SELECT
      t.*,
      m1.body as snippet,
      m1.date_sent as last_msg_date,
      m1.type as type,
      count(m3.id) as unread_count
    FROM
      threads t
    LEFT JOIN
      messages m1 ON (t.id = m1.thread_id)
    LEFT OUTER JOIN
      messages m2 ON (t.id = m2.thread_id AND (m1.date_sent < m2.date_sent OR m1.date_sent = m2.date_sent AND m1.id < m2.id))
    LEFT OUTER JOIN
      messages m3 ON (t.id = m3.thread_id AND (m3.read = false))
    WHERE
      m2.id IS NULL
    group by
      t.id, m1.id
    ;
    """)
  end

  def down do
    execute("DROP VIEW threads_view;")
  end
end
