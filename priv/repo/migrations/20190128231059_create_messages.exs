defmodule RemoteSms.Repo.Migrations.CreateMessages do
  use Ecto.Migration

  def change do
    create table(:messages) do
      add :original_id, :integer, null: false
      add :original_thread_id, :integer, null: false
      add :body, :string
      add :date_sent, :utc_datetime
      add :type, :integer
      add :read, :boolean
      add :thread_id, references(:threads, on_delete: :delete_all) #, null: false

      timestamps()
    end

  end
end
